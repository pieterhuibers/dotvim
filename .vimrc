execute pathogen#infect()

colorscheme monokai-chris
syntax on
filetype indent plugin on
set tabstop=2 
set shiftwidth=2

let g:airline_powerline_fonts = 1
let g:airline_theme='badwolf'

set clipboard=unnamed
set number
set incsearch
set laststatus=2
set tabstop=2
set shiftwidth=2
set nowrap
set showmatch
set cursorline
hi CursorLine   cterm=NONE ctermbg=237
set colorcolumn=160
set textwidth=160
set guifont=Liberation\ Mono\ for\ Powerline

set nobackup
set nowritebackup
set noswapfile
set showcmd
nohlsearch

exec "set listchars=tab:\uBB-,trail:\uB7,nbsp:~,eol:\uAC"

let mapleader = "\<Space>"

noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
inoremap <Up> <NOP>
inoremap <Down> <NOP>
inoremap <Left> <NOP>
inoremap <Right> <NOP>

" Mappings
nmap <leader>v :e ~/.vimrc<CR>
nmap <leader>r :source ~/.vimrc<CR>
nmap <leader>w :w<CR>
nmap <leader>o :CtrlP<CR>
nmap <leader>q :q<CR>
nmap <leader>x :q!<CR>
nmap <C-N> :NERDTreeToggle<CR>
nmap <silent> <leader>n :nohlsearch <CR>
nmap <silent> <leader>c :lcd %:p:h <CR>
nmap <C-T> :tabnew <CR>
nmap <C-h> :tabp<CR>
nmap <C-l> :tabn<CR>
nmap <C-N> :NERDTreeToggle <CR>
nmap <C-/> gcc
vmap <C-/> gc
nmap <leader>a :%s/\s\+$// \| nohlsearch<CR>
vmap <C-_> gc
nnoremap <F2> :set nonumber!<CR>
nnoremap <leader>l :set list!<CR>
nmap <S-Space> a
imap <S-Space> <ESC>
nmap ,b :%s/&/\r/g<CR>

" Stay in visual mode when indenting.
vnoremap < <gv
vnoremap > >gv
" " Visual select last pasted text
noremap gp `[v`]
" Make Y behave more like C and D
noremap Y y$
" Easily replace the current word and all its occurrences.
nnoremap <Leader>rc :%s/\<<C-r><C-w>\>/
vnoremap <Leader>rc y:%s/<C-r>"/
nnoremap <Leader>cc :%s/\<<C-r><C-w>\>/<C-r><C-w>
vnoremap <Leader>cc y:%s/<C-r>"/<C-r>"

let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"

let g:syntastic_always_populate_loc_list = 1

"
" ctags optimization
set autochdir
set tags=tags;
