This repository contains the configuration files for my vim setup

Installation
===
```
git clone https://gitlab.com/pieterhuibers/dotvim ~/.vim
ln -s ~/.vim/.vimrc ~/.vimrc
cd ~/.vim/bundle
git submodule init
git submodule update
```

Install powerline fonts
---
```
git clone https://github.com/powerline/fonts ~/tmpfonts
~/tmpfonts/install.sh
rm -rf ~/tmpfonts
```
